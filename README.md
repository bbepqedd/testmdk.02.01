# TestMDK.02.01



## Description project:

Создать настольное приложение: "Строительный магазин"

Создать ТЗ на разработку приложения

Создать ERD-диаграмму в Visio

Создать USE_CASE

## Technical task

https://gitlab.com/bbepqedd/testmdk.02.01/-/blob/main/13.09.docx

## LeryaMerlen_bak

https://gitlab.com/bbepqedd/testmdk.02.01/-/blob/main/LeryaMerlen_3ISP11-20.bak

## USE_CASE

https://gitlab.com/bbepqedd/testmdk.02.01/-/blob/main/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%821__1_.vsdx

## ERD
https://gitlab.com/bbepqedd/testmdk.02.01/-/blob/main/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2023-09-14_144427.png

## Диаграмма БД
https://gitlab.com/bbepqedd/testmdk.02.01/-/blob/main/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2023-09-27_170019.png

## Optika20:
ERD:
https://gitlab.com/bbepqedd/testmdk.02.01/-/blob/main/ERD_Optika20.vsdx

USE_CASE:
https://gitlab.com/bbepqedd/testmdk.02.01/-/blob/main/USE_CASE_Optika20.vsdx




